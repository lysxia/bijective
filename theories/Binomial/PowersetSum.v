From Coq Require Import Arith.
From Bijective Require Import
  Core
  Sum
  Powerset
  Binomial.Def.

(* The powerset is equivalent to the disjoint union of its subsets
   over every size k. *)
Lemma pow_union_subsets :
  forall n, pow n <~> disjoint_union (S n) (fun k => subsets n k).
Proof.
  induction n.
  - cbn. apply symmetric_bijection, unleft_bij.
  - eapply transitive_bijection; [ apply pow_succ | ].
    eapply symmetric_bijection.
    eapply transitive_bijection; [ eapply disjoint_union_S | ].
    cbn [ subsets ].
    eapply transitive_bijection; [ eapply sum_bij; [ apply reflexive_bijection | apply disjoint_union_sum ] | ].
    apply symmetric_bijection.
    eapply transitive_bijection; [ | apply sum_assoc_bij ].
    apply sum_bij; [ | apply IHn ].
    cbn.
    eapply transitive_bijection; [ | eapply sum_bij; [ apply reflexive_bijection | eapply sum_bij; [ apply symmetric_bijection, subsets_n_Sn; constructor | apply reflexive_bijection ] ]].
    eapply transitive_bijection; [ | eapply sum_bij; [ apply reflexive_bijection | apply symmetric_bijection, unright_bij ]].
    replace unit with (subsets n O) by apply subsets_n_O.
    eapply transitive_bijection; [ | apply disjoint_union_S ].
    assumption.
Defined.

Theorem pow_binom :
  forall n,
    2 ^ n = nsum (S n) (fun k => binom n k).
Proof.
  intros n.
  (* 2 ^ n = sum (1+n) (fun k => binom n k). *)
  apply fin_eq.
  (* fin (2 ^ n) <~> fin (sum (1+n) (fun k => binom n k)). *)
  eapply transitive_bijection.
  1:{ apply fin_pow. }
  (* pow n <~> fin (sum (1+n) (fun k => binom n k)). *)
  eapply transitive_bijection.
  2:{ apply disjoint_union_nsum. }
  (* pow n <~> disjoint_union (1+n) (fun k => fin (binom n k)). *)
  eapply transitive_bijection.
  2:{ apply disjoint_union_cong. apply subsets_binom. }
  (* pow n <~> disjoint_union (1+n) (fun k => subsets n k). *)
  apply pow_union_subsets.
Qed.
