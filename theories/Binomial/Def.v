From Coq Require Import Arith.
From Bijective Require Import Core.

(* Binomial coefficient. *)
Fixpoint binom (n k : nat) : nat :=
  match k, n with
  | O, _ => 1
  | S _, O => O
  | S j, S m => binom m k + binom m j
  end.

(* The set of subsets of (fin n) with size k. *)
Fixpoint subsets (n k : nat) : Set :=
  match k, n with
  | O, _ => unit
  | S _, O => False
  | S j, S m => subsets m k + subsets m j
  end.

Example binom_4_2 : binom 4 2 = 6.
Proof. reflexivity. Qed.

(* The size of the set of subsets of size k is a binomial coefficient. *)
Lemma subsets_binom :
  forall n k, subsets n k <~> fin (binom n k).
Proof.
  induction n; intros []; cbn.
  - apply symmetric_bijection, option_False_unit.
  - apply reflexive_bijection.
  - apply symmetric_bijection, option_False_unit.
  - eapply transitive_bijection; [ eapply sum_bij; apply IHn | ].
    apply symmetric_bijection, fin_sum.
Defined.

Fixpoint extend_subsets_false (n k : nat)
  : subsets n k -> subsets (S n) k :=
  match k, n with
  | O, _=> fun _ => tt
  | S _, O => fun y => match y with end
  | S j, S m => fun y => inl y
  end.

Definition extend_subsets_true (n k : nat)
  : subsets n k -> subsets (S n) (S k) :=
  fun y => inr y.

Lemma subsets_n_O n : subsets n O = unit.
Proof.
  destruct n; reflexivity.
Defined.

Lemma binom_n_Sn n : forall m, n < m -> binom n m = 0.
Proof.
  induction n; intros m []; cbn.
  - reflexivity.
  - reflexivity.
  - rewrite 2 IHn; try reflexivity.
    + apply Nat.lt_succ_diag_r.
    + apply Nat.lt_lt_succ_r.
      apply Nat.lt_succ_diag_r.
  - rewrite 2 IHn; try reflexivity.
    + apply Nat.lt_succ_l; assumption.
    + apply Nat.lt_lt_succ_r.
      apply Nat.lt_succ_l. assumption.
Defined.

Lemma subsets_n_Sn n m : n < m -> subsets n m <~> False.
Proof.
  intros I.
  eapply transitive_bijection; [ apply subsets_binom | ].
  rewrite (binom_n_Sn _ _ I).
  apply reflexive_bijection.
Defined.
