From Coq Require Import Arith.
From Bijective.Core Require Import
  Bijection
  CoreFacts.

(* "Canonical" set of n elements. *)
Fixpoint fin (n : nat) : Type :=
  match n with
  | O => False
  | S m => option (fin m)
  end.

(* [fin] sets are characterized by their cardinality. *)
Lemma fin_eq : forall n m, (fin n <~> fin m) -> n = m.
Proof.
  induction n; intros [].
  - reflexivity.
  - intros [f f' _]; destruct (f' None).
  - intros [f f' _]; destruct (f None).
  - cbn. intros I. apply inv_option_bij in I. apply IHn in I.
    f_equal; assumption.
Defined.

Lemma eq_fin : forall n m, n = m -> (fin n <~> fin m).
Proof.
  intros ? ? []; apply reflexive_bijection.
Defined.

Lemma fin_sum : forall n m, fin (n + m) <~> (fin n + fin m).
Proof.
  induction n; intros m; cbn.
  - apply symmetric_bijection, unright_bij.
  - eapply transitive_bijection; [| apply symmetric_bijection, option_sum_left_bij].
    apply option_bij. trivial.
Defined.
