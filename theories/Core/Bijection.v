Record bijective {X Y : Type} (f : X -> Y) (f' : Y -> X) :=
  { injectivity : forall x, f' (f x) = x
  ; surjectivity : forall y, f (f' y) = y
  }.

(* (X <~> Y) is a proposition meaning:
   There exists a bijection between X and Y. *)
Record bijection (X Y : Type) : Type :=
  { bijection_to : X -> Y
  ; bijection_from : Y -> X
  ; bijection_bijectivity : bijective bijection_to bijection_from
  }.

(* (bijection X Y) is also denoted (X <~> Y) *)
Infix "<~>" := bijection (at level 70).

Lemma reflexive_bijection : forall X, X <~> X.
Proof.
  intros X. exists (fun x => x) (fun x => x). constructor; reflexivity.
Defined.

Lemma symmetric_bijection : forall X Y, X <~> Y -> Y <~> X.
Proof.
  intros X Y [f f' [f'f ff']]; exists f' f; constructor; auto.
Defined.

(* If there is a bijection between X and Y, and a bijection between Y and Z,
   then there is a bijection between X and Z. *)
Lemma transitive_bijection :
  forall X Y Z : Type,
    (X <~> Y) -> (Y <~> Z) -> (X <~> Z).
Proof.
  intros X Y Z [f f' [ Hf Hf' ]] [g g' [ Hg Hg' ]].
  exists (fun x => g (f x)) (fun z => f' (g' z)).
  split; intros.
  - rewrite Hg, Hf. reflexivity.
  - rewrite Hf', Hg'. reflexivity.
Defined.

Arguments transitive_bijection {X Y Z}.
