From Bijective.Core Require Import
  Bijection.

Lemma option_False_unit : option False <~> unit.
Proof.
  exists (fun _ => tt) (fun _ => None).
  constructor; intros []; reflexivity + contradiction.
Defined.

Lemma bool_times_bij : forall A, bool * A <~> (A + A).
Proof.
  intros A.
  exists
    (fun ba =>
      match fst ba with
      | true => inl (snd ba)
      | false => inr (snd ba)
      end)
    (fun aa =>
      match aa with
      | inl a => (true, a)
      | inr a => (false, a)
      end).
  constructor; [ intros [[]] | intros [] ]; reflexivity.
Defined.

Definition unleft {A} (a' : A + False) : A :=
  match a' with
  | inl a => a
  | inr v => match v with end
  end.

Definition unright {A} (a' : False + A) : A :=
  match a' with
  | inl v => match v with end
  | inr a => a
  end.

Lemma unleft_bij {A} : A + False <~> A.
Proof.
  exists unleft inl. constructor; [ intros [| []] | ]; try reflexivity.
Defined.

Lemma unright_bij {A} : False + A <~> A.
Proof.
  exists unright inr. constructor; [ intros [[] |] | ]; try reflexivity.
Defined.

Definition sum_assoc_l {A B C} (abc : (A + B) + C) : A + (B + C) :=
  match abc with
  | inl (inl a) => inl a
  | inl (inr b) => inr (inl b)
  | inr c => inr (inr c)
  end.

Definition sum_assoc_r {A B C} (abc : A + (B + C)) : (A + B) + C :=
  match abc with
  | inl a => inl (inl a)
  | inr (inl b) => inl (inr b)
  | inr (inr c) => inr c
  end.

Lemma sum_assoc_bij {A B C} : (A + B) + C <~> A + (B + C).
Proof.
  exists sum_assoc_l sum_assoc_r.
  constructor; [ intros [[] | ] | intros [| []] ]; reflexivity.
Defined.

Definition sum_comm {A B} (ab : A + B) : B + A :=
  match ab with
  | inl a => inr a
  | inr b => inl b
  end.

Lemma sum_comm_bij {A B} : A + B <~> B + A.
Proof.
  exists sum_comm sum_comm.
  constructor; intros []; reflexivity.
Defined.

Definition option_sum_left_l {A B} (oab : option A + B) : option (A + B) :=
  match oab with
  | inl None => None
  | inl (Some a) => Some (inl a)
  | inr b => Some (inr b)
  end.

Definition option_sum_left_r {A B} (oab : option (A + B)) : option A + B :=
  match oab with
  | None => inl None
  | Some (inl a) => inl (Some a)
  | Some (inr b) => inr b
  end.

Lemma option_sum_left_bij {A B} : option A + B <~> option (A + B).
Proof.
  exists option_sum_left_l option_sum_left_r.
  constructor; [ intros [[] | ] | intros [[] | ] ]; reflexivity.
Defined.

Definition sum_map {A B C D} (f : A -> C) (g : B -> D) (ab : A + B) : C + D :=
  match ab with
  | inl a => inl (f a)
  | inr b => inr (g b)
  end.

Lemma sum_bij {A B C D} : A <~> C -> B <~> D -> (A + B) <~> (C + D).
Proof.
  intros [f f' [f'f ff']] [g g' [g'g gg']].
  exists (sum_map f g) (sum_map f' g').
  split; intros []; cbn; f_equal; trivial.
Defined.

Definition option_map {A B} (f : A -> B) (oa : option A) : option B :=
  match oa with
  | None => None
  | Some a => Some (f a)
  end.

Lemma option_bij : forall A B, A <~> B -> option A <~> option B.
Proof.
  intros A B [f f' [ f'f ff' ]].
  exists (option_map f) (option_map f').
  split; intros []; cbn; f_equal; trivial.
Defined.

Section SWAP.

Context {A B : Type}.
Context (f : option A -> option B).
Context (f' : option B -> option A).
Context (f'f : forall a, f' (f a) = a).

Definition swap_contra :
  forall (a : A)
         (fNone : f None = None)
         (fSome : f (Some a) = None),
  False.
Proof.
  congruence.
Defined.

Context (a : A).

Definition option_split {A} (o : option A) :
  { a | o = Some a } + { o = None } :=
  match o with
  | Some a => inleft (exist _ a eq_refl)
  | None => inright eq_refl
  end.

Definition swap : B :=
  match option_split (f (Some a)) with
  | inleft (exist _ b _) => b
  | inright fSome =>
    match option_split (f None) with
    | inleft (exist _ b0 _) => b0
    | inright fNone => match swap_contra _ fNone fSome with end
    end
  end.

End SWAP.

Lemma inv_swap :
  forall A B (f : option A -> option B) (f' : option B -> option A)
    (f'f : forall a, f' (f a) = a)
    (ff' : forall b, f (f' b) = b)
    (a : A),
    swap f' f ff' (swap f f' f'f a) = a.
Proof.
  intros. unfold swap at 2.
  destruct option_split as [ [b fSome] | fSome ].
  - unfold swap.
    destruct option_split as [ [a' f'Some] | f'Some ]; congruence.
  - destruct option_split as [ [b0 fNone] | fNone ].
    + unfold swap.
      destruct option_split as [ [a' f'Some] | f'Some ].
      * congruence.
      * destruct option_split as [ [a0 f'None ] | f'None ]; congruence.
    + congruence.
Defined.

Lemma inv_option_bij : forall A B, option A <~> option B -> A <~> B.
Proof.
  intros A B [f f' [ f'f ff' ]].
  exists (swap f f' f'f) (swap f' f ff').
  split; apply inv_swap.
Defined.
