From Coq Require Import Arith.
From Bijective Require Import Core.

(* The expression (disjoint_union n (fun i => f i)) denotes
   the disjoint union of (f i) for i from 0 to (n-1). *)
Fixpoint disjoint_union (n : nat) (f : nat -> Type) : Type :=
  match n with
  | O => False
  | S m => f m + disjoint_union m f
  end.

(* The expression (nsum n (fun i => f i)) denotes
   the sum of (f i) for i from 0 to (n-1). *)
Fixpoint nsum (n : nat) (f : nat -> nat) : nat :=
  match n with
  | O => O
  | S m => f m + nsum m f
  end.

(* Example: sum of i for i from 0 to (n-1) is equal to (n * (n + 1) / 2).*)
Example triangular_nsum :
  forall n, 2 * nsum n (fun i => i) = n * (n - 1).
Proof.
  induction n.
  - reflexivity. (* Base case: just compute, both sides evaluate to 0. *)
  - cbn [ nsum ]. rewrite Nat.mul_add_distr_l. rewrite IHn.
    destruct n; cbn.
    + reflexivity.
    + rewrite Nat.sub_0_r, Nat.add_0_r, Nat.mul_succ_r, !Nat.add_succ_r.
      rewrite (Nat.add_comm (_ * _)). cbn.
      rewrite <- Nat.add_assoc.
      reflexivity.
Defined.
(* TODO Prove it bijectively *)

(* The cardinality of a disjoint union is the sum of each
   component's cardinality. *)
Lemma disjoint_union_nsum :
  forall n f, disjoint_union n (fun i => fin (f i)) <~> fin (nsum n f).
Proof.
  induction n; intros f; cbn.
  - apply reflexive_bijection.
  - eapply transitive_bijection; [ eapply sum_bij; [ apply reflexive_bijection | apply IHn ] | ].
    apply symmetric_bijection, fin_sum.
Defined.

(* If the components (f k) and (g k) of two disjoint unions
   are pairwise-equivalent,
   then the disjoint sums are equivalent. *)
Lemma disjoint_union_cong :
  forall n f g,
    (forall k, f k <~> g k) ->
    disjoint_union n f <~> disjoint_union n g.
Proof.
  intros n f g fg; induction n.
  - apply reflexive_bijection.
  - cbn; apply sum_bij; trivial.
Defined.

Lemma disjoint_union_S n (f : nat -> Type) :
  disjoint_union (S n) f <~> f O + disjoint_union n (fun k => f (S k)).
Proof.
  induction n; cbn.
  - apply reflexive_bijection.
  - eapply transitive_bijection; [ | apply sum_assoc_bij ].
    eapply transitive_bijection; [ | eapply sum_bij; [ apply sum_comm_bij | apply reflexive_bijection ] ].
    eapply transitive_bijection; [ | apply symmetric_bijection, sum_assoc_bij ].
    apply sum_bij; [ apply reflexive_bijection | apply IHn  ].
Defined.

Lemma disjoint_union_sum n (f g : nat -> Type) :
  disjoint_union n (fun k => (f k + g k)%type) <~> disjoint_union n f + disjoint_union n g.
Proof.
  induction n; cbn.
  - apply symmetric_bijection, unleft_bij.
  - eapply transitive_bijection; [ eapply sum_bij; [ apply reflexive_bijection | apply IHn] | ].
    eapply transitive_bijection; [ apply sum_assoc_bij | ].
    eapply transitive_bijection; [ eapply sum_bij; [ apply reflexive_bijection | apply symmetric_bijection, sum_assoc_bij ] | ].
    eapply transitive_bijection; [ eapply sum_bij; [ apply reflexive_bijection | eapply sum_bij; [ apply sum_comm_bij | apply reflexive_bijection ] ] | ].
    eapply transitive_bijection; [ eapply sum_bij; [ apply reflexive_bijection | apply sum_assoc_bij ] | ].
    apply symmetric_bijection, sum_assoc_bij.
Defined.
