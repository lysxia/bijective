From Coq Require Import Arith.
From Bijective Require Import Core.

(** * Definition *)

(* "powerset" of (fin n) *)
Fixpoint pow (n : nat) : Type :=
  match n with
  | O => unit
  | S m => bool * pow m
  end.

(* There is a bijection between
   (fin (2 ^ n)) ("canonical" set with (2 ^ n) elements)
   and (pow n) ("powerset" of (fin n)). *)
Lemma fin_pow :
  forall n, fin (2 ^ n) <~> pow n.
Proof.
  induction n; cbn.
  - apply option_False_unit.
  - rewrite Nat.add_0_r.
    eapply transitive_bijection; [ | apply symmetric_bijection, bool_times_bij ].
    eapply transitive_bijection; [ apply fin_sum | ].
    apply sum_bij; assumption.
Defined.

(** * Properties *)

(* The empty set is a member of all powersets. *)
Fixpoint empty_pow (n : nat) : pow n :=
  match n with
  | O => tt
  | S m => (false, empty_pow m)
  end.

Lemma pow_succ : forall n, pow (S n) <~> pow n + pow n.
Proof.
  intros; apply bool_times_bij.
Defined.
